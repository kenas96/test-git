import { Component, OnInit } from "@angular/core";

@Component({
  selector: "main-issue",
  template: "<router-outlet></router-outlet>",
})
export class IssueMainComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
